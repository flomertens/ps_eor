#!/usr/bin/env python

from __future__ import print_function
from __future__ import absolute_import


import os
import sys
from argparse import ArgumentParser

import numpy as np

from ps_eor import datacube, psutil, sphcube, flagger

parser = ArgumentParser(description="Load h5 visibilities cube image, transform to alm cube and save")
parser.add_argument('files', help='Input Stoke I h5 visibility cube(s) files', nargs='+')
parser.add_argument('--output_postfix', '-p', help='Output postfix added to original filename',
                    required=False, default=None)
parser.add_argument('--output_dir', '-o', help='Output filename', required=False, default='')
parser.add_argument('--umin', help='Min baseline (in lambda)', default=0, type=float)
parser.add_argument('--umax', help='Max baseline (in lambda)', default=1000, type=float)
parser.add_argument('--nside', help='Nside (if not set, default to 1/3 of lmax', default=None, type=int)
parser.add_argument('--flagfile', '-f', help='Flag to be applied to visibility cube before transformation',
                    default=None, type=str)


def main():
    args = parser.parse_args(sys.argv[1:])

    flag = None
    if args.flagfile is not None:
        flag = flagger.Flag.load(args.flagfile)

    print('Options: umin=%s, umax=%s' % (args.umin, args.umax))

    for i, file in enumerate(args.files):
        print('Converting %s (%s / %s) ...' % (file, i + 1, len(args.files)))
        cube = datacube.CartDataCube.load(file)

        if flag is not None:
            print('Applying flag ...')
            cube = flag.apply(cube)

        cube.filter_uvrange(args.umin, args.umax)

        lmax = int(np.floor(2 * np.pi * cube.ru.max()))

        nside = args.nside
        if nside is None:
            nside = 2 ** (np.ceil(np.log2(lmax / 3.)))
        nside = int(nside)

        sph_cube = sphcube.SphDataCube.from_cartcube(cube, nside, lmax)

        out_name = os.path.basename(file)

        if args.output_postfix is not None:
            out_name = psutil.append_postfix(out_name, args.output_postfix)

        sph_cube.save(os.path.join(args.output_dir, out_name))


if __name__ == '__main__':
    main()
