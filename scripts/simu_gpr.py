#!/usr/bin/env python

from __future__ import print_function
from __future__ import absolute_import

import sys
import imp

import numpy as np
import pandas as pd

from ps_eor import psutil, datacube, pspec, fgfit
from ps_eor import fitutil, simu


class PsStacker(object):

    def __init__(self):
        self.all_ps = []

    def add(self, ps):
        self.all_ps.append(ps)

    def get(self):
        ps2d = self.all_ps[0]
        m = np.mean([ps.data for ps in self.all_ps], axis=0)
        std = psutil.robust_std([ps.data for ps in self.all_ps], axis=0)
        return pspec.CylindricalPowerSpectra(m, std, ps2d.delay, ps2d.el, ps2d.k_per, ps2d.k_par)


def store_table(table, params, simu_gpr, kbins):
    ps3d_sub = simu_gpr.get_ps3d_sub(kbins).data
    ps3d_noise = simu_gpr.get_ps3d_noise(kbins).data
    ps3d_eor = simu_gpr.get_ps3d_eor(kbins).data
    ps3d_eor_rec = simu_gpr.get_ps3d_eor_rec(kbins).data
    ps3d_fg_err = simu_gpr.get_ps3d_fg_err(kbins).data

    k_mean = simu_gpr.get_ps3d_sub(kbins).k_mean

    p = np.repeat([params], len(k_mean), axis=0)
    r = np.vstack([k_mean, ps3d_sub, ps3d_noise, ps3d_eor, ps3d_eor_rec, ps3d_fg_err]).T

    for line in np.hstack([p, r]):
        table.loc[len(table)] = line


def do_simu(config):
    el = 2 * np.pi * np.arange(config.umin, config.umax, 8)
    ps_conf = pspec.PowerSpectraConfig(el)
    ps_conf.cov_err_n_samples = config.cov_err_n_samples
    ps_conf.weights_by_default = True
    ps_conf.psf_weights_square = True
    pb = datacube.PrimaryBeam(config.pb_type, lambda f: config.pb_fwhm)

    ps_gen = pspec.PowerSpectraCart(config.eor_bin, ps_conf, pb)

    simu_gen = simu.SimulationGenerator(config.fg_gen, config.fg_med_gen, config.eor_gen,
                                        config.noise_gen)

    gpr_config = fitutil.GprConfig.load(config.gpr_fit_config)
    fitter = fgfit.GprForegroundFit(gpr_config)

    if config.gpr_pre_fit_config is not None:
        gpr_pre_config = fitutil.GprConfig.load(config.gpr_pre_fit_config)
        pre_fitter = fgfit.GprForegroundFit(gpr_pre_config)
    else:
        pre_fitter = None

    cols_name = ['ls_fg', 'ls_eor', 'var_fg', 'var_eor', 'ls_eor_rec', 'var_eor_rec',
                 'log_lik_eor`', 'log_lik_no_eor', 'k', 'ps_sub', 'ps_noise', 'ps_eor',
                 'ps_rec_eor', 'ps_fg_err']
    table_large = pd.DataFrame(columns=cols_name)
    table_small = pd.DataFrame(columns=cols_name)
    ps_eor_rec1 = PsStacker()
    ps_eor_rec2 = PsStacker()

    pt = psutil.progress_tracker(config.n_simu)
    n_no_detection = 0

    for i in range(config.n_simu):
        ls_fg = config.ls_fg.get()
        ls_eor = config.ls_eor.get()
        var_fg = config.var_fg.get()
        var_eor = config.var_eor.get()

        if var_eor == 0:
            var_eor_test = 0.05 ** 2
        else:
            var_eor_test = 0.01 * var_eor

        simu_gpr = simu.SimuFitter(simu_gen, ps_gen, pre_fitter, fitter)
        log_lik_no_eor, log_lik_eor = simu_gpr.run(ls_fg, var_fg, ls_eor, var_eor, test_no_eor=config.test_no_eor)
        ls_res, var_res = simu_gpr.get_eor_model()

        ps3d_sub = simu_gpr.get_ps3d_sub(config.kbins_l).data
        ps3d_noise = simu_gpr.get_ps3d_noise(config.kbins_l).data
        ps3d_eor = simu_gpr.get_ps3d_eor(config.kbins_l).data
        ps3d_eor_rec = simu_gpr.get_ps3d_eor_rec(config.kbins_l).data

        print('Res: %s | %.2f -> %.2f | %.2f -> %.2f -- %s' % (ls_fg, ls_eor, ls_res, np.sqrt(var_eor),
                                                               np.sqrt(var_res), pt(i)))

        print('  Ratio I-V:', ', '.join(['%.2f' % k for k in (ps3d_sub - ps3d_noise) / ps3d_eor]))
        print('  Ratio EoR:', ', '.join(['%.2f' % k for k in ps3d_eor_rec / ps3d_eor]))
        print('  Evidence:', log_lik_no_eor, log_lik_eor, log_lik_eor > log_lik_no_eor)

        no_detection = (log_lik_no_eor > log_lik_eor) or var_res < var_eor_test

        if no_detection:
            print(' -> No detection')
            n_no_detection += 1

        params = [ls_fg, ls_eor, var_fg, var_eor, ls_res, var_res, log_lik_eor, log_lik_no_eor]
        store_table(table_large, params, simu_gpr, config.kbins_l)
        store_table(table_small, params, simu_gpr, config.kbins_s)

        ps_eor_rec1.add(ps_gen.get_ps2d(simu_gpr.gpr_res.get_eor_model()))
        ps_eor_rec2.add(ps_gen.get_ps2d(simu_gpr.gpr_res.sub) - ps_gen.get_ps2d(simu_gpr.simu.noise))

    for table in [table_large, table_small]:
        table['ratio1'] = table['ps_rec_eor'] / table['ps_eor']
        table['ratio2'] = (table['ps_sub'] - table['ps_noise']) / table['ps_eor']

    table_small.to_csv(config.output_name + '_small.csv', index=False)
    table_large.to_csv(config.output_name + '_large.csv', index=False)

    ps_eor_rec1.get().save_to_txt(config.output_name + '_ps_eor_rec1.txt')
    ps_eor_rec2.get().save_to_txt(config.output_name + '_ps_eor_rec2.txt')

    print('\nGlobal results:')
    print(table_large.groupby('k')[['ratio1', 'ratio2']].mean())
    print(table_large.groupby('k')[['ratio1', 'ratio2']].std())

    print('\nNumber of no detection: %s (%.1f %%)' % (n_no_detection, n_no_detection / config.n_simu * 100.))


def main():
    config = imp.load_source('config', sys.argv[1])
    do_simu(config)


if __name__ == '__main__':
    main()
