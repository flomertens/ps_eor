#!/usr/bin/env python

from __future__ import print_function
from __future__ import absolute_import

import os
import sys
from argparse import ArgumentParser

import matplotlib.pyplot as plt
import matplotlib as mpl

import numpy as np

from scipy.stats import binned_statistic

from ps_eor import datacube, pspec, fitutil, fgfit, psutil, simu


parser = ArgumentParser(description="Run GPR Injection test")
parser.add_argument('--file_i', '-i', help='Stokes I visibility cube in h5 format', required=True)
parser.add_argument('--file_v', '-v', help='Stokes V visibility cube in h5 format', required=True)
parser.add_argument('--gpr_config_i', '-ci', help='GPR configuration parset for Stokes I', required=True)
parser.add_argument('--gpr_config_v', '-cv', help='GPR configuration parset for Stokes V', required=True)
parser.add_argument('--umin', help='Min baseline (in lambda)', default=0, type=float)
parser.add_argument('--umax', help='Max baseline (in lambda)', default=1e99, type=float)
parser.add_argument('--eor_bin', '-e', help='The EoR redshift bin', required=True, type=str)
parser.add_argument('--eor_bins_list', '-l', help='Listing of EoR redshift bins', default='eor_bins.txt')
parser.add_argument('--output_dir', '-o', help='Output directory', default='.')
parser.add_argument('--du', help='PS baseline resolution', default=8, type=float)
parser.add_argument('--numbers', '-n', help='Number of simulations', default=50, type=int)
parser.add_argument('--threshold_outliers', help='Threshold for outliers filtering. Default is 6.',
                    default=6, type=float)

mpl.rcParams['image.cmap'] = 'rainbow'
mpl.rcParams['image.origin'] = 'lower'
mpl.rcParams['image.interpolation'] = 'nearest'
mpl.rcParams['axes.grid'] = True

ratio_sefd = 0.7
ratio_weights = 0.4
min_sefd = 2500


def plot_ratios(ratios, param, filename, nbins=0):
    gb = ratios.groupby(param)

    plt.figure()
    x = gb[param].mean().values
    m = gb.ratio.median().values - 1
    std = (gb.ratio.quantile(0.84) - gb.ratio.quantile(0.16)) / 2.

    if nbins > 0:
        mb, _, _ = binned_statistic(x, m, bins=nbins)
        std, _, _ = binned_statistic(x, m, 'std', bins=nbins)
        x, _, _ = binned_statistic(x, x, bins=nbins)
        m = mb

    plt.fill_between(x, m - std, m + std,
                     alpha=0.5, color=psutil.orange)
    plt.plot(x, m, color=psutil.orange)

    if param == 'k':
        plt.xscale('log')

    plt.ylabel('Fractional bias')
    plt.xlabel(param)

    plt.tight_layout()
    plt.ylim(-1.2, 1.2)
    plt.savefig(filename)


def main():
    args = parser.parse_args(sys.argv[1:])

    assert os.path.isfile(args.file_i)
    assert os.path.isfile(args.file_v)
    assert os.path.isfile(args.gpr_config_i)
    assert os.path.isfile(args.gpr_config_v)

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    print('Loading data ...')
    ft_i_cube = datacube.CartDataCube.load(args.file_i)
    ft_v_cube = datacube.CartDataCube.load(args.file_v)

    ft_i_cube, ft_v_cube = datacube.get_common_cube(ft_i_cube, ft_v_cube)

    ft_i_cube.filter_uvrange(args.umin, args.umax)
    ft_v_cube.filter_uvrange(args.umin, args.umax)

    eor_bin_list = pspec.EorBinList.load(args.eor_bins_list)

    eor = eor_bin_list.get(args.eor_bin, freqs=ft_i_cube.freqs)

    el = 2 * np.pi * (np.arange(ft_i_cube.ru.min(), ft_i_cube.ru.max(), args.du))
    ps_conf = pspec.PowerSpectraConfig(el)
    ps_conf.window_fct = 'boxcar'
    ps_conf.psf_weights_square = True

    pb = datacube.LofarHBAPrimaryBeam()

    ps_gen = pspec.PowerSpectraCart(eor, ps_conf, pb)

    gpr_config_i = fitutil.GprConfig.load(args.gpr_config_i)
    gpr_config_v = fitutil.GprConfig.load(args.gpr_config_v)

    gpr_fit_i = fgfit.GprForegroundFit(gpr_config_i)
    gpr_fit_v = fgfit.GprForegroundFit(gpr_config_v)

    gpr_fit_v.config.verbose = False
    gpr_fit_i.config.verbose = False
    gpr_fit_i.config.num_restarts = 20
    simu_inj = simu.SimuEorInjector('test', eor, gpr_fit_i, gpr_fit_v, ft_i_cube, ft_v_cube,
                                    eor_ls_range=[0.4, 0.8],
                                    eor_var_range=[0.04, 0.15])

    print('Initial run ...')
    simu_inj.initial_run()
    gpr_fit_i.config.verbose = False
    kbins = np.logspace(np.log10(ps_gen.kmin), np.log10(0.5), num=10, base=10)

    print('Run simulations')
    simu_inj.run(args.numbers, ps_gen, kbins)

    simu_inj.generate_ratios(ps_gen, kbins)

    # simu_inj.ratios = pd.read_csv(os.path.join(args.output_dir, 'ratios.csv'))

    print('Fractional relative difference:')
    print(simu_inj.ratios.groupby('k').ratio.median().values - 1)

    plot_ratios(simu_inj.ratios, 'k', os.path.join(args.output_dir, 'ratios_k.pdf'))
    plot_ratios(simu_inj.ratios, 'eor_ls', os.path.join(args.output_dir, 'ratios_ls_eor.pdf'), nbins=8)
    plot_ratios(simu_inj.ratios, 'eor_var', os.path.join(args.output_dir, 'ratios_var_eor.pdf'), nbins=8)

    simu_inj.ratios.to_csv(os.path.join(args.output_dir, 'ratios.csv'), index=False)


if __name__ == '__main__':
    main()
