#!/usr/bin/env python

from __future__ import print_function
from __future__ import absolute_import


import sys
from argparse import ArgumentParser
from argparse import RawTextHelpFormatter

import numpy as np

import astropy.units as u

from ps_eor import datacube, psutil

parser = ArgumentParser(description="Load fits natural PSF fits, transform to weight cube and save",
                        formatter_class=RawTextHelpFormatter)
parser.add_argument('files', help='Input natural PSF fits files', nargs='+')
parser.add_argument('--output_name', '-o', help='Output filename', required=True)
parser.add_argument('--theta_fov', help='Trim PSF to FoV (in degrees)', default=None, type=float)
parser.add_argument('--umin', help='Min baseline (in lambda)', default=40, type=float)
parser.add_argument('--umax', help='Max baseline (in lambda)', default=300, type=float)
parser.add_argument('--int_time', '-i', help='Integration time of a single visibility',
                    required=True, type=str)
parser.add_argument('--total_time', '-t', help='Total time of observation',
                    required=True, type=str)


def main():
    args = parser.parse_args(sys.argv[1:])

    int_time = u.Quantity(args.int_time, u.s)
    total_time = u.Quantity(args.total_time, u.s)

    opts = (args.umin, args.umax, int_time, total_time, args.theta_fov)
    print('Options: umin=%s, umax=%s, int_time=%s, total_time=%s, theta_fov=%s' % opts)
    print('Loading %s files ...' % len(args.files))

    if args.theta_fov is not None:
        args.theta_fov = np.radians(args.theta_fov)

    files = psutil.sort_by_fits_key(args.files, 'CRVAL3')

    w_cube = datacube.CartWeightCube.load_from_fits_psf(files, args.umin,
                                                        args.umax, int_time.to(u.s).value,
                                                        total_time.to(u.s).value, theta_fov=args.theta_fov)

    print('Saving to: %s' % args.output_name)
    w_cube.save(args.output_name)


if __name__ == '__main__':
    main()
