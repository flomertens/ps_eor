#!/usr/bin/env python

from __future__ import print_function
from __future__ import absolute_import

import sys
from argparse import ArgumentParser
from argparse import RawTextHelpFormatter

import numpy as np

from ps_eor import datacube, psutil

parser = ArgumentParser(description="Load fits image, transform to visibilities cube and save",
                        formatter_class=RawTextHelpFormatter)
parser.add_argument('files', help='Input fits files', nargs='+')
parser.add_argument('--output_name', '-o', help='Output filename', required=True)
parser.add_argument('--theta_fov', help='Trim image to FoV (in degrees)', default=4, type=float)
parser.add_argument('--umin', help='Min baseline (in lambda)', default=50, type=float)
parser.add_argument('--umax', help='Max baseline (in lambda)', default=250, type=float)
parser.add_argument('--no_kelvin_conversion', help='Do not convert to Kelvin', action='store_true')
parser.add_argument('--imager_scale_factor', '-s', help='Imager scale factor for PSF->px conversion. \
                    If not set, it will use whatever is in the header', default=0, type=float)
parser.add_argument('--compat_wscnormf', help="Compatibility for old/incorrect WSCNORMF:\n"
                    " - old_normpsf: for normalizepsf < 16/06/2017\n"
                    " - old_wsclean: for wsclean < 16/06/2017\n",
                    default=None)


def main():
    args = parser.parse_args(sys.argv[1:])

    print('Options: umin=%s, umax=%s, theta_fov=%s' % (args.umin, args.umax, args.theta_fov))
    print('Loading %s files ...' % len(args.files))

    if args.imager_scale_factor == 0:
        args.imager_scale_factor = None

    files = psutil.sort_by_fits_key(args.files, 'CRVAL3')

    data_cube = datacube.CartDataCube.load_from_fits_image(files, args.umin,
                                                           args.umax, np.radians(args.theta_fov),
                                                           convert_jy2k=not args.no_kelvin_conversion,
                                                           imager_scale_factor=args.imager_scale_factor,
                                                           compat_wscnormf=args.compat_wscnormf)

    print('Saving to: %s' % args.output_name)
    data_cube.save(args.output_name)


if __name__ == '__main__':
    main()
