#!/usr/bin/env python

from __future__ import print_function
from __future__ import absolute_import

import os
import sys
from argparse import ArgumentParser

import matplotlib as mpl
mpl.use('Agg')

from ps_eor import datacube, psutil, flagger

parser = ArgumentParser(description="Run flagger on cube and save flag data")
parser.add_argument('--file_i', '-i', help='Input Stoke I h5 visibility cube file(s)', required=True)
parser.add_argument('--file_v', '-v', help='Input Stoke V h5 visibility cube file(s)', required=True)
parser.add_argument('--output_dir', '-o', help='Output directory', default='')
parser.add_argument('--flag_config', '-f', help='GPR configuration parset for Stokes I', required=True)

mpl.rcParams['image.cmap'] = 'Spectral_r'
mpl.rcParams['image.origin'] = 'lower'
mpl.rcParams['image.interpolation'] = 'nearest'
mpl.rcParams['axes.grid'] = True


def main():
    args = parser.parse_args(sys.argv[1:])

    assert os.path.isfile(args.file_i)
    assert os.path.isfile(args.file_v)
    assert os.path.isfile(args.flag_config)

    print('Loading cube ...')
    i_cube = datacube.CartDataCube.load(args.file_i)
    v_cube = datacube.CartDataCube.load(args.file_v)

    print('Running flagger ...')
    flagger_runner = flagger.FlaggerRunner.load(args.flag_config)
    flagger_runner.run(i_cube, v_cube)

    out_flag_name = psutil.append_postfix(os.path.basename(args.file_i), 'flag')
    out_flag_plot_name = psutil.append_postfix(os.path.basename(args.file_i), 'flag').replace('.h5', '.pdf')

    print('Saving flags to: %s ...' % out_flag_name)
    fig = flagger_runner.plot()
    fig.savefig(os.path.join(args.output_dir, out_flag_plot_name))
    flagger_runner.flag.save(os.path.join(args.output_dir, out_flag_name))

    print('All done!')


if __name__ == '__main__':
    main()
