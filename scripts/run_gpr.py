#!/usr/bin/env python

from __future__ import print_function
from __future__ import absolute_import

import os
import sys
import time
from argparse import ArgumentParser

import numpy as np

import matplotlib as mpl
mpl.use('Agg')

import matplotlib.pyplot as plt

from ps_eor import datacube, pspec, fitutil, fgfit, psutil, flagger


parser = ArgumentParser(description="Run GPR & generate power spectra")
parser.add_argument('--file_i', '-i', help='Stokes I visibility cube in h5 format', required=True)
parser.add_argument('--file_v', '-v', help='Stokes V visibility cube in h5 format', required=True)
parser.add_argument('--gpr_config_i', '-ci', help='GPR configuration parset for Stokes I', required=True)
parser.add_argument('--gpr_config_v', '-cv', help='GPR configuration parset for Stokes V', required=True)
parser.add_argument('--flag_config', '-f', help='GPR configuration parset for Stokes I', required=True)
parser.add_argument('--eor_bins_list', '-e', help='Listing of EoR redshift bins', required=True)
parser.add_argument('--ps_conf', '-p', help='Power spectra configuration file', required=True)
parser.add_argument('--output_dir', '-o', help='Output directory', default='.')
parser.add_argument('--plots_output_dir', '-po', help='Output directory', required=True, default=None)
parser.add_argument('--rnd_seed', help='Set a random seed', default=3, type=int)

mpl.rcParams['image.cmap'] = 'Spectral_r'
mpl.rcParams['image.origin'] = 'lower'
mpl.rcParams['image.interpolation'] = 'nearest'
mpl.rcParams['axes.grid'] = True


def do_gpr(i_cube, v_cube, gpr_config_i, gpr_config_v, rnd_seed=1):
    t = time.time()
    np.random.seed(rnd_seed)

    gpr_config_v = fitutil.GprConfig.load(gpr_config_v)
    gpr_config_i = fitutil.GprConfig.load(gpr_config_i)

    print('Running GPR for Stokes V ...\n')

    gpr_fit = fgfit.GprForegroundFit(gpr_config_v)
    gpr_res_v = gpr_fit.run(v_cube, v_cube, rnd_seed=rnd_seed)

    print("\nDone in %.2f s\n" % (time.time() - t))

    noise_v = v_cube.make_diff_cube_interp()

    if gpr_config_i.use_simulated_noise:
        sefd = gpr_res_v.sub.new_with_cov_err().estimate_sefd()
        print('Using simulated noise with sefd = %.1f Jy' % sefd)
        weights = v_cube.weights.copy()
        weights.scale_with_noise_cube(noise_v, sefd_poly_fit_deg=2, expected_sefd=sefd, scale_freqs=False)

        noise_i = weights.simulate_noise(sefd, v_cube.meta.total_time,
                                         fake_apply_win_fct=True, hermitian=False)
        noise_i.weights = v_cube.weights.copy()
    else:
        noise_i = gpr_res_v.sub.new_with_cov_err()

    gpr_fit = fgfit.GprForegroundFit(gpr_config_i)

    do_scaled_noise = not gpr_config_i.fixed_noise and gpr_config_i.heteroscedastic

    if do_scaled_noise:
        gpr_config_i.fixed_noise_scale = 1

        t = time.time()
        print('Testing noise-scale value...\n')

        gpr_res = gpr_fit.run(i_cube, noise_i, rnd_seed=rnd_seed)

        n_optimize_loop = gpr_config_i.noise_scale_search_n_restarts
        tol = gpr_config_i.noise_scale_search_tol
        rmin, rmax = gpr_config_i.noise_scale_search_range
        method = gpr_config_i.noise_scale_search_method
        maxiter = gpr_config_i.noise_scale_search_maxiter

        print(method)

        noise_scale = gpr_res.test_single_parameter('het_Gauss.variance', rmin, rmax, tol=tol,
                                                    n_optimize_loop=n_optimize_loop,
                                                    method=method, brute_ns=maxiter, verbose=True)

        print('New noise_scale:', noise_scale)
        noise_i = np.sqrt(noise_scale) * noise_i

        print("\nDone in %.2f s\n" % (time.time() - t))

    t = time.time()
    print('Running GPR for Stokes I ...\n')

    gpr_res = gpr_fit.run(i_cube, noise_i, rnd_seed=rnd_seed)

    print("\nDone in %.2f s\n" % (time.time() - t))

    return gpr_res, gpr_res_v, noise_i


def plot_img_and_freq_slice(eor, data_cube, ax1, ax2, name, theta_fov=None):
    img_cube = eor.get_slice(data_cube).regrid().image()

    img_cube.plot(ax=ax1, auto_scale_quantiles=(0.1, 0.999))
    img_cube.plot_slice(ax=ax2)

    ax1.text(0.03, 0.94, name, transform=ax1.transAxes, ha='left', fontsize=10)
    ax2.text(0.03, 0.94, name, transform=ax2.transAxes, ha='left', fontsize=10)


def save_img_and_ps(ps_gen, eor, kbins, cmpts, names, filename_img, filename_ps, cycler=None):
    fig, axs = plt.subplots(ncols=2, nrows=len(cmpts), figsize=(11, 3.5 * len(cmpts)))
    fig2, axs2 = plt.subplots(ncols=2, nrows=2, figsize=(10, 7))
    axs2 = axs2.flatten()

    if cycler is None:
        cycler = mpl.rcParams['axes.prop_cycle'] * mpl.cycler('linestyle', ['-'])

    for i, (cmpt, name, prop) in enumerate(zip(cmpts, names, cycler)):
        plot_img_and_freq_slice(eor, cmpt, axs[i, 0], axs[i, 1], name)

        ps_gen.get_ps3d(kbins, cmpt).plot(ax=axs2[0], label=name, c=prop['color'], ls=prop['linestyle'])
        ps_gen.get_ps2d(cmpt).plot_kpar(ax=axs2[1], label=name, c=prop['color'], ls=prop['linestyle'])
        ps_gen.get_ps2d(cmpt).plot_kper(ax=axs2[2], label=name, c=prop['color'], ls=prop['linestyle'])
        ps_gen.get_variance(cmpt).plot(ax=axs2[3], label=name, c=prop['color'], ls=prop['linestyle'])

    lgd = fig2.legend(*axs2[0].get_legend_handles_labels(),
                      bbox_to_anchor=(0.5, 1.02), loc="upper center", ncol=len(cmpts))

    fig.tight_layout()
    fig2.tight_layout()
    fig.savefig(filename_img)
    fig2.savefig(filename_ps, bbox_extra_artists=(lgd,), bbox_inches='tight')


def save_ps2d(ps_gen, ps, ps2d, title, filename, **kargs):
    fig, (ax1, ax2) = plt.subplots(ncols=2, figsize=(10, 4))
    ps.plot(ax=ax1, **kargs)
    ps2d.plot(ax=ax2, wedge_lines=[45, 90], z=ps_gen.eor.z, **kargs)
    fig._plotutils_colorbars[ax2].ax.set_xlabel(r'$\mathrm{K^2\,h^{-3}\,cMpc^3}$')

    ax1.text(0.03, 0.92, title, transform=ax1.transAxes, ha='left', fontsize=11)
    ax2.text(0.03, 0.92, title, transform=ax2.transAxes, ha='left', fontsize=11)
    fig.tight_layout()
    fig.savefig(filename)


def get_ratios(ps_gen, ps2d_ratio):
    ratio = np.median(ps2d_ratio.data)
    ratio_high = np.median((ps2d_ratio.data)[ps_gen.k_par > 0.8])
    ratio_low = np.median((ps2d_ratio.data)[ps_gen.k_par < 0.6])

    return ratio, ratio_high, ratio_low


def main():
    args = parser.parse_args(sys.argv[1:])

    if args.plots_output_dir is None:
        args.plots_output_dir = args.output_dir

    assert os.path.isfile(args.file_i)
    assert os.path.isfile(args.file_v)
    assert os.path.isfile(args.flag_config)
    assert os.path.isfile(args.eor_bins_list)
    assert os.path.isfile(args.ps_conf)

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    if not os.path.exists(args.plots_output_dir):
        os.makedirs(args.plots_output_dir)

    print('Loading data ...')
    i_cube = datacube.DataCube.load(args.file_i)
    v_cube = datacube.DataCube.load(args.file_v)

    ps_builder = pspec.PowerSpectraBuilder(args.ps_conf, args.eor_bins_list)
    ps_conf = ps_builder.ps_config

    # i_cube.filter_uvrange(ps_conf.umin, ps_conf.umax)
    # v_cube.filter_uvrange(ps_conf.umin, ps_conf.umax)

    print('Running flagger ...')
    flagger_runner = flagger.FlaggerRunner.load(args.flag_config)
    i_cube, v_cube = flagger_runner.run(i_cube, v_cube)

    ps_conf.el = 2 * np.pi * np.arange(i_cube.ru.min(), i_cube.ru.max(), ps_conf.du)

    for eor_name in ps_builder.eor_bin_list.get_all_names():
        out_dir = os.path.join(args.output_dir, 'eor%s_u%s-%s' % (eor_name, int(ps_conf.umin), int(ps_conf.umax)))
        plot_out_dir = os.path.join(args.plots_output_dir, 'eor%s_u%s-%s' %
                                    (eor_name, int(ps_conf.umin), int(ps_conf.umax)))

        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        if not os.path.exists(plot_out_dir):
            os.makedirs(plot_out_dir)

        ps_gen = ps_builder.get(i_cube, eor_name)
        ps_gen_fg = ps_builder.get(i_cube, eor_name, window_fct='hann', rmean_freqs=True)
        eor = ps_gen.eor

        if ps_conf.empirical_weighting:
            sefd = v_cube.make_diff_cube().estimate_sefd()
            poly_deg = ps_conf.empirical_weighting_polyfit_deg
            v_cube.weights.scale_with_noise_cube(eor.get_slice(
                v_cube.make_diff_cube()), sefd_poly_fit_deg=poly_deg, expected_sefd=sefd)
            i_cube.weights.scale_with_noise_cube(eor.get_slice(
                v_cube.make_diff_cube()), sefd_poly_fit_deg=poly_deg, expected_sefd=sefd)

        kbins = np.logspace(np.log10(ps_gen.kmin), np.log10(ps_conf.kbins_kmax), ps_conf.kbins_n)

        print('\nRunning GPR for EoR bin:', eor.name)
        print('Frequency range: %.2f - %.2f MHz (%i SB)\n' % (eor.fmhz[0], eor.fmhz[-1], len(eor.fmhz)))
        print('Mean redshift: %.2f (%.2f MHz)\n' % (eor.z, eor.mfreq * 1e-6))

        print('Saving GPR result...\n')

        gpr_res, gpr_res_v, noise_i = do_gpr(eor.get_slice_fg(i_cube), eor.get_slice_fg(v_cube),
                                             args.gpr_config_i, args.gpr_config_v, rnd_seed=args.rnd_seed)

        gpr_res_v.save(out_dir, 'gpr_res_V')
        gpr_res.save(out_dir, 'gpr_res_I')
        noise_i.save(os.path.join(out_dir, 'gpr_res_I.noise.h5'))

        # Saving residuals I and V PS
        ps2d_i_res = ps_gen.get_ps2d(gpr_res.sub)
        ps2d_v_res = ps_gen.get_ps2d(gpr_res_v.sub)
        ps_i_res = ps_gen.get_ps(gpr_res.sub)
        ps_v_res = ps_gen.get_ps(gpr_res_v.sub)
        ps3d_i_res = ps_gen.get_ps3d(kbins, gpr_res.sub)
        ps3d_v_res = ps_gen.get_ps3d(kbins, gpr_res_v.sub)
        ps3d_noise_i = ps_gen.get_ps3d(kbins, noise_i)
        ps3d_rec = ps_gen.get_ps3d_with_noise(kbins, gpr_res.sub, noise_i)

        ratio, ratio_high, ratio_low = get_ratios(ps_gen, ps2d_i_res / ps2d_v_res)
        ratio_txt = 'Ratio I / V (med: %.2f / %.2f / %.2f)' % (ratio, ratio_high, ratio_low)
        print(ratio_txt)

        ps2d_i_res.save_to_txt(os.path.join(out_dir, 'ps2d_I_residual.txt'))
        ps2d_v_res.save_to_txt(os.path.join(out_dir, 'ps2d_V_residual.txt'))

        ps3d_i_res.save_to_txt(os.path.join(out_dir, 'ps3d_I_residual.txt'))
        ps3d_v_res.save_to_txt(os.path.join(out_dir, 'ps3d_V_residual.txt'))
        ps3d_rec.save_to_txt(os.path.join(out_dir, 'ps3d_I_minus_V.txt'))
        ps3d_noise_i.save_to_txt(os.path.join(out_dir, 'ps3d_I_noise.txt'))

        up = ps3d_rec.get_upper()
        k_up = ps3d_rec.k_mean[np.nanargmin(up)]
        upper_limit = '2 sigma upper limit: (%.1f)^2 mK^2 at k = %.3f' % (np.nanmin(up), k_up)
        print(upper_limit)

        print('Plotting results ...\n')

        # Plotting GPR results
        save_ps2d(ps_gen, ps_i_res, ps2d_i_res, 'Stokes I residual',
                  os.path.join(plot_out_dir, 'ps2d_I_residual.pdf'))
        save_ps2d(ps_gen, ps_v_res, ps2d_v_res, 'Stokes V residual',
                  os.path.join(plot_out_dir, 'ps2d_V_residual.pdf'))

        save_ps2d(ps_gen, ps_i_res / ps_v_res, ps2d_i_res / ps2d_v_res, 'I residual / V residual',
                  os.path.join(plot_out_dir, 'ps2d_I_over_V_residual.pdf'), vmin=1, vmax=10)

        save_ps2d(ps_gen, ps_i_res / ps_v_res, ps2d_i_res / ps_gen.get_ps2d(noise_i), 'I residual / noise',
                  os.path.join(plot_out_dir, 'ps2d_I_over_noise.pdf'), vmin=0.5, vmax=10)

        cmpts = [i_cube, v_cube, gpr_res.fit, gpr_res.sub, gpr_res_v.sub]
        names = ['I', 'V', 'I fg', 'I residual', 'V residual']
        cycler = mpl.cycler('color', [psutil.lblue, psutil.lgreen, psutil.red, psutil.dblue, psutil.dgreen]) \
            + mpl.cycler('linestyle', ['-', '-', ':', '-', '-'])
        save_img_and_ps(ps_gen_fg, eor, kbins, cmpts, names,
                        os.path.join(plot_out_dir, 'img_I_V_residuals.pdf'),
                        os.path.join(plot_out_dir, 'ps_I_V_residuals.pdf'), cycler)

        cmpts = [gpr_res.pre_fit, gpr_res.get_fg_model(), gpr_res.get_eor_model(), gpr_res.sub, noise_i]
        names = ['fg int', 'fg mix', 'eor', 'I residual', 'noise I']

        if gpr_res.post_fit.data.mean() != 0:
            cmpts.append(gpr_res.post_fit)
            names.append('fg pca')

        save_img_and_ps(ps_gen_fg, eor, kbins, cmpts, names,
                        os.path.join(plot_out_dir, 'img_gpr_cmpts.pdf'),
                        os.path.join(plot_out_dir, 'ps_gpr_cmpts.pdf'))

        # PS3D plot
        fig, ax = plt.subplots()
        ps3d_i_res.plot(label='I residual', ax=ax, nsigma=2, c=psutil.dblue)
        ps3d_noise_i.plot(label='noise I', ax=ax, nsigma=2, c=psutil.green)
        ps3d_rec.plot(label='I residual - noise', ax=ax, nsigma=2, c=psutil.orange)

        ax.legend()
        fig.savefig(os.path.join(plot_out_dir, 'ps3d.pdf'), bbox_inches='tight')


if __name__ == '__main__':
    main()
