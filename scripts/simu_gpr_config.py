import numpy as np
import GPy

from ps_eor import simu, pspec

from __future__ import print_function
from __future__ import absolute_import


freqs = np.arange(130.1, 149.1, 0.2) * 1e6
eor_bin_list = pspec.EorBinList(freqs)
eor_bin_list.add_freq(1, 133, 146, 132, 147)
eor_bin = eor_bin_list.get(1)

kbins_s = np.logspace(np.log10(0.075), np.log10(0.6), 11)
kbins_l = [0, 0.2, 0.4, 0.6]

theta_fov = np.radians(6.014)
umin = 50
umax = 250
res = np.radians(1.17 / 60.)

pb_fwhm = np.radians(40)
pb_type = 'tophat'

fg_file = '/home/flo/data/simu_lofar/fg/fg_standard.fits'
fg_gen = simu.SimuFromImageCube(fg_file, 115e6, 0.5e6, res,
                                freqs, theta_fov, umin, umax, pb_fwhm)

fg_med_gen = simu.SimuGPCube(freqs, GPy.kern.Matern32(1), res, theta_fov, umin, umax)

eor_gen = simu.SimuGPCube(freqs, GPy.kern.Exponential(1), res, theta_fov, umin, umax)

w_file = '/home/flo/data/simu_lofar/noise/lofar_u40-300/weights_lofar_u50-250_fov6_int60s_tot12h.h5'
n_nights = 100
scale_factor = np.sqrt(3.6 * 0.5)
noise_gen = simu.SimuNoiseFromWeightCube(w_file, 4000, n_nights * 12 * 3600, theta_fov, umin, umax, scale_factor)

ls_eor = simu.Constant(0.75)
ls_fg = simu.Constant(3)
var_fg = simu.Constant(3)
var_eor = simu.Constant(0.05)

gpr_fit_config = 'gpr_config_full.parset'
gpr_pre_fit_config = None

n_simu = 20
cov_err_n_samples = 10

output_name = 'simu_result_test'
