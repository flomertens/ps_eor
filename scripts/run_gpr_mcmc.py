#!/usr/bin/env python

from __future__ import print_function
from __future__ import absolute_import

import os
import sys
from argparse import ArgumentParser

import matplotlib.pyplot as plt
import matplotlib as mpl

import numpy as np

import GPy
import emcee
import corner

from ps_eor import datacube, pspec, fitutil, fgfit, psutil, simu


parser = ArgumentParser(description="Run GPR MCMC")
parser.add_argument('--file_i', '-i', help='Stokes I visibility cube in h5 format', required=True)
parser.add_argument('--file_v', '-v', help='Stokes V visibility cube in h5 format', required=True)
parser.add_argument('--gpr_config_i', '-ci', help='GPR configuration parset for Stokes I', required=True)
parser.add_argument('--gpr_config_v', '-cv', help='GPR configuration parset for Stokes V', required=True)
parser.add_argument('--umin', help='Min baseline (in lambda)', default=0, type=float)
parser.add_argument('--umax', help='Max baseline (in lambda)', default=1e99, type=float)
parser.add_argument('--eor_bin', '-e', help='The EoR redshift bin', required=True, type=str)
parser.add_argument('--eor_bins_list', '-l', help='Listing of EoR redshift bins', default='eor_bins.txt')
parser.add_argument('--output_dir', '-o', help='Output directory', default='.')
parser.add_argument('--du', help='PS baseline resolution', default=8, type=float)
parser.add_argument('--v_eor', '-ve', help='Eor injected', default=0, type=float)
parser.add_argument('--numbers', '-n', help='Number of simulations', default=50, type=int)
parser.add_argument('--threshold_outliers', help='Threshold for outliers filtering. Default is 6.',
                    default=6, type=float)

mpl.rcParams['image.cmap'] = 'rainbow'
mpl.rcParams['image.origin'] = 'lower'
mpl.rcParams['image.interpolation'] = 'nearest'
mpl.rcParams['axes.grid'] = True


class UniformPrior(object):

    domain = GPy.priors._REAL

    def __init__(self, l, u):
        self.lower = l
        self.upper = u

    def __str__(self):
        return "[{:.2g}, {:.2g}]".format(self.lower, self.upper)

    def lnpdf(self, x):
        region = (x >= self.lower) * (x <= self.upper)
        return np.log(region * np.e)

    def lnpdf_grad(self, x):
        return np.zeros(x.shape)

    def rvs(self, n):
        return np.random.uniform(self.lower, self.upper, size=n)


class PsStacker(object):

    def __init__(self):
        self.all_ps = []

    def add(self, ps):
        self.all_ps.append(ps)

    # def get(self):
    #     ps2d = self.all_ps[0]
    #     m = np.mean([ps.data for ps in self.all_ps], axis=0)
    #     std = psutil.robust_std([ps.data for ps in self.all_ps], axis=0)
    #     return pspec.CylindricalPowerSpectra(m, std, ps2d.delay, ps2d.el, ps2d.k_per, ps2d.k_par)

    def get(self):
        ps3d = self.all_ps[0]
        m = np.mean([ps.data for ps in self.all_ps], axis=0)
        std = np.std([ps.data for ps in self.all_ps], axis=0)
        return pspec.SphericalPowerSpectra(m, std, ps3d.k_bins, ps3d.k_mean)


def main():
    args = parser.parse_args(sys.argv[1:])

    assert os.path.isfile(args.file_i)
    assert os.path.isfile(args.file_v)
    assert os.path.isfile(args.gpr_config_i)
    assert os.path.isfile(args.gpr_config_v)

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    print('Loading data ...')
    ft_i_cube = datacube.CartDataCube.load(args.file_i)
    ft_v_cube = datacube.CartDataCube.load(args.file_v).new_with_cov_err()

    ft_i_cube, ft_v_cube = datacube.get_common_cube(ft_i_cube, ft_v_cube)

    eor_cube_gen = simu.SimuGPCube.from_cube(ft_i_cube, GPy.kern.Exponential(1))
    # res = np.radians(70.3 / 3600.)
    # shape = (308, 308)
    # theta_fov = res * shape[0]

    # eor_file = '/home/flo/data/simu_lofar/eor/eor_standard.fits'
    # eor_cube_gen = simu.SimuFromImageCube(eor_file, 115e6, 0.45e6, res,
    #                                  ft_i_cube.freqs, theta_fov, args.umin, args.umax, None)

    v = args.v_eor * ft_v_cube.data.var()
    ls = 0.9
    eor_cube = eor_cube_gen.get(v, ls)
    eor_cube = ft_i_cube.new_with_data(eor_cube.data[:, :len(ft_i_cube.uu)])
    _, eor_cube = datacube.get_common_cube(ft_i_cube, eor_cube)

    ft_i_cube = eor_cube + ft_i_cube

    ft_i_cube.filter_uvrange(args.umin, args.umax)
    ft_v_cube.filter_uvrange(args.umin, args.umax)

    eor_bin_list = pspec.EorBinList.load(args.eor_bins_list)

    eor = eor_bin_list.get(args.eor_bin, freqs=ft_i_cube.freqs)

    ft_v_cube.weights.scale_with_noise_cube(eor.get_slice(ft_v_cube))
    ft_i_cube.weights.scale_with_noise_cube(eor.get_slice(ft_v_cube))

    el = 2 * np.pi * (np.arange(ft_i_cube.ru.min(), ft_i_cube.ru.max(), args.du))
    ps_conf = pspec.PowerSpectraConfig(el, window_fct=None)
    ps_conf.rmean_axis = None
    ps_conf.weights_by_default = True
    ps_conf.psf_weights_square = False
    ps_conf.weights_with_cov_err = False
    ps_conf.cov_err_n_samples = 1

    pb = datacube.LofarHBAPrimaryBeam()

    ps_gen = pspec.PowerSpectraCart(eor, ps_conf, pb)
    kbins = np.logspace(np.log10(ps_gen.kmin), np.log10(0.6), num=10)

    gpr_config_i = fitutil.GprConfig.load(args.gpr_config_i)

    gpr_fit_i = fgfit.GprForegroundFit(gpr_config_i)
    gpr_res_i = gpr_fit_i.run(eor.get_slice_fg(ft_i_cube), eor.get_slice_fg(ft_v_cube))

    model = gpr_res_i.gpr_res.model.copy()

    # Currently this is a bit of a hack
    # model.kern.fg_Mat32.variance.unconstrain()
    # model.kern.fg_Mat32.variance.set_prior(UniformPrior(0.6, 1.5))
    # model.kern.fg_Mat32.lengthscale.unconstrain()
    # model.kern.fg_Mat32.lengthscale.set_prior(UniformPrior(1.0, 1.7))

    model.kern.eor_Exponential.variance.unconstrain()
    model.kern.eor_Exponential.variance.set_prior(UniformPrior(0, 0.1))
    model.kern.eor_Exponential.lengthscale.unconstrain()
    model.kern.eor_Exponential.lengthscale.set_prior(UniformPrior(0.01, 1.2))

    # model.kern.eor_Exponential_1.variance.unconstrain()
    # model.kern.eor_Exponential_1.variance.set_prior(UniformPrior(0, 0.2))
    # model.kern.eor_Exponential_1.lengthscale.unconstrain()
    # model.kern.eor_Exponential_1.lengthscale.set_prior(UniformPrior(0.4, 1.2))

    # model.het_Gauss.variance_scale.unconstrain()
    # model.het_Gauss.variance_scale.set_prior(UniformPrior(0.8, 2.5))

    # model.kern.int_fg_Mat52.variance.unconstrain()
    # model.kern.int_fg_Mat52.variance.set_prior(UniformPrior(0.1, 1))
    # model.kern.int_fg_Mat52.lengthscale.unconstrain()
    # model.kern.int_fg_Mat52.lengthscale.set_prior(UniformPrior(5, 200))

    model.kern.fg_RatQuad.variance.unconstrain()
    model.kern.fg_RatQuad.variance.set_prior(UniformPrior(0.1, 2))
    model.kern.fg_RatQuad.lengthscale.unconstrain()
    model.kern.fg_RatQuad.lengthscale.set_prior(UniformPrior(0.01, 5))
    model.kern.fg_RatQuad.power.unconstrain()
    model.kern.fg_RatQuad.power.set_prior(UniformPrior(0.001, 10))

    # model.kern.eor_GammaExp.variance.unconstrain()
    # model.kern.eor_GammaExp.variance.set_prior(UniformPrior(0, 0.3))
    # model.kern.eor_GammaExp.lengthscale.unconstrain()
    # model.kern.eor_GammaExp.lengthscale.set_prior(UniformPrior(0.1, 2))
    # model.kern.eor_GammaExp.power.unconstrain()
    # model.kern.eor_GammaExp.power.set_prior(UniformPrior(0.1, 1))

    def lnprob(params):
        try:
            model.optimizer_array = params
            if np.isnan(model.log_prior()):
                return -np.inf

            return model.log_likelihood() + model.log_prior()
        except np.linalg.LinAlgError:
            return -np.inf

    ndim = len(model.optimizer_array)
    nwalkers = 100
    nsteps = 200
    pos = []
    for i in range(nwalkers):
        model.randomize()
        pos.append(model.optimizer_array.copy())

    print(model)

    sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob, a=0.2)

    pr = psutil.progress_report(nsteps)
    for i, result in enumerate(sampler.sample(pos, iterations=nsteps)):
        pr(i)

    samples = sampler.chain[:, 100:, :].reshape((-1, ndim))

    ps_stacker = PsStacker()
    pick = np.random.randint(0, samples.shape[0], 500)
    for i in pick:
        model.optimizer_array = samples[i]
        # sub = gpr_res_i.run_on_cube(eor.get_slice_fg(ft_i_cube) - gpr_res_i.pre_fit, model=model).sub
        sub = gpr_res_i.run_on_cube(eor.get_slice_fg(ft_i_cube), model=model).sub
        ps_stacker.add(ps_gen.get_ps3d(kbins, sub))

    ps_stacker.get().save_to_txt(os.path.join(args.output_dir, 'mcmc_ps_sub_eor%.2f.txt' % args.v_eor))
    ps_gen.get_ps3d(kbins, eor_cube).save_to_txt(os.path.join(args.output_dir, 'mcmc_ps_eor_eor%.2f.txt' % args.v_eor))

    np.save(os.path.join(args.output_dir, 'mcmc_samples.npy'), samples)

    labels = [r'$ var_{\mathrm{med}}$', r'$l_{\mathrm{med}}$',
              r'$ var_{\mathrm{21}}$', r'$l_{\mathrm{21}}$',
              r'$ var_{\mathrm{21_2}}$', r'$l_{\mathrm{21_2}}$']
    labels += ['excess_noise']
    # ranges = [(2, 6), (0.8, 1.4), (0.1, 1.4), (0, 1.2)]
    # ranges += [(0.8, 2)]
    levels = 1.0 - np.exp(-0.5 * np.arange(1, 2.1, 1) ** 2)
    # t_sig_fg = (fg_med_cube.data / gpr_res.sub.cov_err.data_scale).std() / var_noise ** 0.5
    # t_sig_eor = (eor_cube.data / gpr_res.sub.cov_err.data_scale).std() / var_noise ** 0.5

    fig, axs = plt.subplots(ncols=ndim, nrows=ndim)
    # fig.set_size_inches((3.3, 2.8))
    corner.corner(samples, fig=fig, labels=labels, color=psutil.black, max_n_ticks=3, no_fill_contours=True,
                  show_titles=True, plot_datapoints=False, smooth=1, levels=levels, range=None,
                  contour_kwargs={'colors': psutil.black, 'linewidths': 0.8})
    fig.subplots_adjust(bottom=0.13, left=0.125, right=0.9, top=0.85)
    for ax in axs.flatten():
        ax.grid(True)

    fig.savefig(os.path.join(args.output_dir, 'mcmc_eor%.2f.pdf' % args.v_eor))


if __name__ == '__main__':
    main()
