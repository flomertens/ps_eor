#!/usr/bin/env python

from __future__ import print_function
from __future__ import absolute_import


import sys

from argparse import ArgumentParser
from argparse import RawTextHelpFormatter

import numpy as np

import matplotlib as mpl

from ps_eor import datacube, psutil


parser = ArgumentParser(description="Compare ",
                        formatter_class=RawTextHelpFormatter)
parser.add_argument('h5_files', help='Input data in h5 format', nargs='+')
parser.add_argument('--umin', help='Min baseline (in lambda)', default=75, type=float)
parser.add_argument('--umax', help='Max baseline (in lambda)', default=250, type=float)
parser.add_argument('--outliers', help='Filter outliers', action='store_true')

mpl.rcParams['image.cmap'] = 'rainbow'
mpl.rcParams['image.origin'] = 'lower'
mpl.rcParams['image.interpolation'] = 'nearest'
mpl.rcParams['axes.grid'] = True


def open_cube(file, umin, umax, outliers=True):
    cart_cube = datacube.CartDataCube.load(file)

    cart_cube.filter_uvrange(umin, umax)

    if outliers:
        outliers = psutil.get_outliers(cart_cube.freqs, cart_cube.data, threshold_n_rms=10)
        print('Filtering outliers:', cart_cube.freqs[outliers] * 1e-6)
        cart_cube.filter_outliers(outliers)

    return cart_cube


def main():
    args = parser.parse_args(sys.argv[1:])

    first = open_cube(args.h5_files[0], args.umin, args.umax, args.outliers)
    freqs = first.freqs

    for file in args.h5_files[1:]:
        cube = open_cube(file, args.umin, args.umax, args.outliers)
        freqs = np.intersect1d(freqs, cube.freqs)
        print(len(freqs))

    print(' '.join(['%i' % k for k in freqs]))


if __name__ == '__main__':
    main()
