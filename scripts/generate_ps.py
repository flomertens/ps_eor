#!/usr/bin/env python

from __future__ import print_function
from __future__ import absolute_import

import os
import sys
from argparse import ArgumentParser

import matplotlib as mpl
mpl.use('Agg')

import matplotlib.pyplot as plt

import numpy as np

from ps_eor import datacube, pspec, flagger


parser = ArgumentParser(description="Generate Power Spectra")
parser.add_argument('--file_i', '-i', help='Stokes I visibility cube in h5 format', required=True)
parser.add_argument('--file_v', '-v', help='Stokes V visibility cube in h5 format (optional, for flagging purpose)')
parser.add_argument('--flag_config', '-f', help='GPR configuration parset for Stokes I', required=False)
parser.add_argument('--eor_bins_list', '-e', help='Listing of EoR redshift bins', required=True)
parser.add_argument('--ps_conf', '-p', help='Power spectra configuration', required=True)
parser.add_argument('--output_dir', '-o', help='Output directory', default='.')

mpl.rcParams['image.cmap'] = 'Spectral_r'
mpl.rcParams['image.origin'] = 'lower'
mpl.rcParams['image.interpolation'] = 'nearest'
mpl.rcParams['axes.grid'] = True


def main():
    args = parser.parse_args(sys.argv[1:])

    assert os.path.isfile(args.file_i)
    assert os.path.isfile(args.eor_bins_list)
    assert os.path.isfile(args.ps_conf)

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    print('Loading data ...')
    i_cube = datacube.CartDataCube.load(args.file_i)

    if args.file_v:
        v_cube = datacube.CartDataCube.load(args.file_v)

    eor_bin_list = pspec.EorBinList.load(args.eor_bins_list)
    ps_conf = pspec.PowerSpectraConfig.load(args.ps_conf)

    i_cube.filter_uvrange(ps_conf.umin, ps_conf.umax)

    if args.file_v:
        v_cube.filter_uvrange(ps_conf.umin, ps_conf.umax)
    else:
        v_cube = None

    if args.file_v:
        print('Running flagger ...')
        flagger_runner = flagger.FlaggerRunner.load(args.flag_config)
        i_cube, v_cube = flagger_runner.run(i_cube, v_cube)

        fig = flagger_runner.plot()
        fig.savefig(os.path.join(args.output_dir, 'flagger.pdf'))

    ps_conf.el = 2 * np.pi * np.arange(i_cube.ru.min(), i_cube.ru.max(), ps_conf.du)

    for eor in eor_bin_list.get_all(freqs=i_cube.freqs):
        out_suffix = 'eor%s_u%s-%s' % (eor.name, int(ps_conf.umin), int(ps_conf.umax))

        if ps_conf.empirical_weighting and args.file_v:
            v_cube.weights.scale_with_noise_cube(eor.get_slice(
                v_cube), sefd_poly_fit_deg=ps_conf.empirical_weighting_polyfit_deg)
            i_cube.weights.scale_with_noise_cube(eor.get_slice(
                v_cube), sefd_poly_fit_deg=ps_conf.empirical_weighting_polyfit_deg)

        pb = datacube.PrimaryBeam.from_name(ps_conf.primary_beam)

        ps_gen = pspec.PowerSpectraCart(eor, ps_conf, pb)

        print('\nGenerating power spectra for EoR bin:', eor.name)
        print('Frequency range: %.2f - %.2f MHz (%i SB)\n' % (eor.fmhz[0], eor.fmhz[-1], len(eor.fmhz)))
        print('Mean redshift: %.2f (%.2f MHz)\n' % (eor.z, eor.mfreq * 1e-6))

        for cube, stokes in zip([i_cube, v_cube], ['I', 'V']):
            if cube is None:
                continue
            ps = ps_gen.get_ps(cube)
            ps2d = ps_gen.get_ps2d(cube)

            ps.save_to_txt(os.path.join(args.output_dir, 'ps_%s_%s.txt' % (stokes, out_suffix)))
            ps2d.save_to_txt(os.path.join(args.output_dir, 'ps2d_%s_%s.txt' % (stokes, out_suffix)))

            try:
                fig, ax = plt.subplots()
                ps2d.plot(ax, wedge_lines=[90], z=ps_gen.eor.z)
                fig.savefig(os.path.join(args.output_dir, 'ps2d_%s_%s.pdf' % (stokes, out_suffix)))

                fig, ax = plt.subplots()
                ps.plot(ax)
                fig.savefig(os.path.join(args.output_dir, 'ps_%s_%s.pdf' % (stokes, out_suffix)))
            except Exception:
                print('Plotting power-spectra did not succeed. Ignore.')

    print('\nAll done!')


if __name__ == '__main__':
    main()
