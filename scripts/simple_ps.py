#!/usr/bin/env python

from __future__ import print_function
from __future__ import absolute_import

import sys
from argparse import ArgumentParser
from argparse import RawTextHelpFormatter

import matplotlib.pyplot as plt
import matplotlib as mpl

import astropy.units as u

import numpy as np

from ps_eor import datacube, pspec, pscart, psutil

parser = ArgumentParser(description="Generate power spectra",
                        formatter_class=RawTextHelpFormatter)
parser.add_argument('img_list', help='Listing of input fits img')
parser.add_argument('psf_list', help='Listing of input fits psf')
parser.add_argument('--theta_fov', help='Trim image to FoV (in degrees)', default=90, type=float)
parser.add_argument('--umin', help='Min baseline (in lambda)', default=50, type=float)
parser.add_argument('--umax', help='Max baseline (in lambda)', default=250, type=float)
parser.add_argument('--du', help='2D PS baseline resolution', default=8, type=float)
parser.add_argument('--kmax', help='PS max k', default=0.5, type=float)
parser.add_argument('--nk', help='Number of k modes', default=10, type=float)
parser.add_argument('--pb', help="Primary Beam FWHM in degrees. Default to 'lofar' PB", default='lofar')
parser.add_argument('--int_time', '-i', help='Integration time of a single visibility', required=True, type=str)
parser.add_argument('--total_time', '-t', help='Total time of observation', required=True, type=str)
parser.add_argument('--trim_method', '-m', help="Trim method: 'a': after normalization, 'b': before normalization",
                    default='a', type=str)
parser.add_argument('--ps_weighting', '-w', help="Weights visibilities using PSF. Default is to not weights.",
                    action='store_true', default=False)
parser.add_argument('--ps_window_fct', help="Frequency window function. Default is hann",
                    default='hann', type=str)
parser.add_argument('--threshold', help='Filter all visibilities with weights below T times the max weights',
                    default=0.01, type=float)

mpl.rcParams['image.cmap'] = 'rainbow'
mpl.rcParams['image.origin'] = 'lower'
mpl.rcParams['image.interpolation'] = 'nearest'
mpl.rcParams['axes.grid'] = True


def main():
    # Parsing arguments
    args = parser.parse_args(sys.argv[1:])

    int_time = u.Quantity(args.int_time, u.s)
    total_time = u.Quantity(args.total_time, u.s)

    opts = (args.umin, args.umax, int_time, total_time, args.theta_fov, args.trim_method, args.threshold)
    print('Options: umin=%s, umax=%s, int_time=%s, total_time=%s, theta_fov=%s deg, trim_method=%s, threshold=%s' % opts)

    if args.trim_method == 'a' or args.trim_method == 'after':
        trim_method = 'after'
    elif args.trim_method == 'b' or args.trim_method == 'before':
        trim_method = 'before'
    else:
        print("trim_method should either be 'a' or 'b' (see help)")

    # Loading images lists
    imgs = np.loadtxt(args.img_list, dtype=str)
    psfs = np.loadtxt(args.psf_list, dtype=str)

    print('Loading %s files ...' % len(imgs))

    imgs = psutil.sort_by_fits_key(imgs, 'CRVAL3')
    psfs = psutil.sort_by_fits_key(psfs, 'CRVAL3')

    # Creating data cube
    data_cube = datacube.CartDataCube.load_from_fits_image_and_psf(imgs, psfs, args.umin,
                                                                   args.umax, np.radians(args.theta_fov),
                                                                   int_time.to(u.s).value, total_time.to(u.s).value,
                                                                   min_weight_ratio=args.threshold,
                                                                   trim_method=trim_method)

    # Settings PS configuration
    el = 2 * np.pi * (np.arange(data_cube.ru.min(), data_cube.ru.max(), args.du))

    if args.pb == 'lofar':
        pb = datacube.LofarHBAPrimaryBeam()
    else:
        pb = datacube.PrimaryBeam('gaussian', lambda f: np.radians(float(args.pb)))

    if len(data_cube.freqs) > 1:
        eor = pspec.EorBin(1, data_cube.freqs, data_cube.freqs)

        ps_conf = pspec.PowerSpectraConfig(el)
        ps_conf.weights_by_default = args.ps_weighting

        ps_gen = pspec.PowerSpectraCart(eor, ps_conf, pb)

        print('Generating power spectra:')

        print('Frequency range: %.2f - %.2f MHz (%i SBs)' % (eor.fmhz[0], eor.fmhz[-1], len(eor.fmhz)))
        print('Mean redshift: %.2f (%.2f MHz)' % (eor.z, eor.mfreq * 1e-6))

        print('Mean Primary Beam FWHM: %.2f deg' % np.degrees(ps_gen.pb_fwhm))
        print('Channel Width: %.2f MHz' % (eor.df * 1e-6))
        print('Effective Bandwidth: %.2f MHz' % (eor.bw * 1e-6))
        print('Total Bandwidth: %.2f MHz' % (eor.bw_total * 1e-6))

        print('k_par range: %.3f - %.2f' % (ps_gen.k_par[0], ps_gen.k_par[-1]))
        print('k_per range: %.3f - %.2f' % (ps_gen.k_per[0], ps_gen.k_per[-1]))

        print('FoV: %.2f degrees' % np.degrees(data_cube.meta.theta_fov))

        # Generating PSs
        ps = ps_gen.get_ps(data_cube)
        ps.plot(title='Spatial power spectra')
        ps.save_to_txt('power_spectra_spatial.txt')
        plt.tight_layout()
        plt.gcf().savefig('power_spectra_spatial.pdf')

        var = ps_gen.get_variance(data_cube)
        var.plot(title='Variance')
        plt.tight_layout()
        plt.gcf().savefig('variance.pdf')

        ps2d = ps_gen.get_ps2d(data_cube)
        ps2d.plot(title='Cylindrically averaged power spectra')
        ps2d.save_to_txt('power_spectra_2d.txt')
        plt.tight_layout()
        plt.gcf().savefig('power_spectra_2d.pdf')

        kbins = np.logspace(np.log10(ps_gen.kmin), np.log10(args.kmax), args.nk)
        ps3d = ps_gen.get_ps3d(kbins, data_cube)
        ps3d.plot(title='Spherically averaged power spectra')
        ps3d.save_to_txt('power_spectra_3d.txt')
        plt.tight_layout()
        plt.gcf().savefig('power_spectra_3d.pdf')
    else:
        z = psutil.freq_to_z(data_cube.freqs[0])
        freq = data_cube.freqs[0]
        k_per = psutil.l_to_k(el, z)

        print('Generating power spectra')
        print('Frequency: %.2f MHz (z = %.2f)' % (freq * 1e-6, z))
        print('k_per range: %.3f - %.2f' % (k_per[0], k_per[-1]))
        print('Mean Primary Beam FWHM: %.2f deg' % np.degrees(pb.get_fwhm(freq)))
        print('FoV: %.2f degrees' % np.degrees(data_cube.meta.theta_fov))

        ps, ps_err = pscart.get_power_spectra(data_cube.data[0], data_cube.uu, data_cube.vv, el)

        X = psutil.angular_to_comoving_distance(z)
        ps_norm = X ** 2 * pscart.get_cart_pb_corr('gaussian', pb.get_fwhm(freq), data_cube.meta.res,
                                                   data_cube.meta.shape)

        plt.errorbar(k_per, ps * ps_norm, ps_err * ps_norm)
        plt.xlabel(r'$k_{\\bot} (h cMpc^{-1})$')
        plt.ylabel(r'P(k) (K^2 h^{-2}\,cMpc^2})')
        plt.yscale('log')
        plt.gcf().savefig('power_spectra.pdf')


if __name__ == '__main__':
    main()
