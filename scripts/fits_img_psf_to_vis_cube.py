#!/usr/bin/env python

from __future__ import print_function
from __future__ import absolute_import

import sys
from argparse import ArgumentParser
from argparse import RawTextHelpFormatter

import numpy as np

import astropy.units as u

from ps_eor import datacube, psutil

parser = ArgumentParser(description="Load fits and psf image, transform to visibilities cube and save",
                        formatter_class=RawTextHelpFormatter)
parser.add_argument('img_list', help='Listing of input fits img')
parser.add_argument('psf_list', help='Listing of input fits psf')
parser.add_argument('--output_name', '-o', help='Output filename', required=True)
parser.add_argument('--theta_fov', help='Trim image to FoV (in degrees)', default=4, type=float)
parser.add_argument('--umin', help='Min baseline (in lambda)', default=50, type=float)
parser.add_argument('--umax', help='Max baseline (in lambda)', default=250, type=float)
parser.add_argument('--threshold', '-w', help='Filter all visibilities with weights below T times the max weights',
                    default=0.01, type=float, required=False)
parser.add_argument('--int_time', '-i', help='Integration time of a single visibility', required=False,
                    default=None, type=str)
parser.add_argument('--total_time', '-t', help='Total time of observation', required=False, default=None, type=str)
parser.add_argument('--trim_method', '-m', help="Trim method: 'a': after normalization, 'b': before normalization",
                    default='a', type=str)
parser.add_argument('--use_wscnormf', help='Use WSCNORMF to normalize the visibility, and doe not use the PSF',
                    action='store_true', default=False)
parser.add_argument('--compat_wscnormf', help="Compatibility for old/incorrect WSCNORMF:\n"
                    " - old_normpsf: for normalizepsf < 16/06/2017\n"
                    " - old_wsclean: for wsclean < 16/06/2017\n",
                    default=None)
parser.add_argument('--win_function', help='Apply a window function on the trimmed image', default=None)


def main():
    args = parser.parse_args(sys.argv[1:])

    if args.int_time is not None:
        int_time = u.Quantity(args.int_time, u.s).to(u.s).value
    else:
        int_time = None

    if args.total_time is not None:
        total_time = u.Quantity(args.total_time, u.s).to(u.s).value
    else:
        total_time = None

    opts = (args.umin, args.umax, args.theta_fov, args.trim_method, args.threshold)
    print('Options: umin=%s, umax=%s, theta_fov=%s, trim_method=%s, threshold=%s' % opts)
    print('Options: use_wscnormf=%s, compat_wscnormf=%s' % (args.use_wscnormf, args.compat_wscnormf))

    if args.trim_method == 'a' or args.trim_method == 'after':
        trim_method = 'after'
    elif args.trim_method == 'b' or args.trim_method == 'before':
        trim_method = 'before'
    else:
        print("trim_method should either be 'a' or 'b' (see help)")

    imgs = np.loadtxt(args.img_list, dtype=str)
    psfs = np.loadtxt(args.psf_list, dtype=str)

    print('Loading %s files ...' % len(imgs))

    imgs = psutil.sort_by_fits_key(imgs, 'CRVAL3')
    psfs = psutil.sort_by_fits_key(psfs, 'CRVAL3')

    win_fct = None
    if args.win_function:
        name = datacube.WindowFunction.parse_winfct_str(args.win_function)
        win_fct = datacube.WindowFunction(name)
        print('Using mask: %s' % win_fct)

    data_cube = datacube.CartDataCube.load_from_fits_image_and_psf(imgs, psfs, args.umin, args.umax,
                                                                   np.radians(args.theta_fov),
                                                                   int_time=int_time, total_time=total_time,
                                                                   min_weight_ratio=args.threshold,
                                                                   trim_method=trim_method,
                                                                   use_wscnormf=args.use_wscnormf,
                                                                   compat_wscnormf=args.compat_wscnormf,
                                                                   window_function=win_fct)

    print('Saving to: %s' % args.output_name)
    data_cube.save(args.output_name)


if __name__ == '__main__':
    main()
