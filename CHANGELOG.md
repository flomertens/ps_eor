# ChangeLog

## 0.30 - 2024-11-25

 - [New] datacube: add an option to set the datatype when loading images
 - [New] psutil: add cos2_beam() achromatic beam pattern
 - [Fixed] ml_gpr: fix get_scaled_noise_cube()
 - [Fixed] ml_gpr: fix get_residual_cubes() 

## 0.29.3 - 2024-07-09

- [Fixed] pspec: fix latex string formatting issue

## 0.29.2 - 2024-07-09

- [Fixed] Fix a bug in get_scaled_noise_cube()

## 0.29.1 - 2024-07-09

- [Changed] update requirements

## 0.29 - 2024-07-09

- [New] ML-GPR: add uv_max, uv_min, ls_max, uv_break, delay_buffer_us kernel parameters
- [New] ML-GPR: add RatQuad kernel
- [New] Add new option to estimate the noise cube in ML-GPR
- [New] Add pstool run_ml_gpr_inj
- [New] Add use_uv_ps parameter to the MultiStationaryKern class. This allow to set the (diagonal) angular covariance of the kernel to the one from the data (fitted)
- [New] ML-GPR: add uv_weight which use the weights of the visibility to weight visibilities inside each uv_bins
- [New] datacube: add concatenate_datacubes function
- [New] flagger: add an option to the fixed_flagger to flag range of frequency with the condition that variance on range is above a certain ratio of the variance outside the range
- [New] ML-GPR: implement alternative uv binning scheme, parametrized with the uv_bins_n_uni parameter.
- [New] obssimu: add DEx telescop. Thanks Sonia !
- [New] ML-GPR: allow to select a list of kernel separeted by ; in get_kern_part()
- [New] ML-GPR: add methods to retrieve component cubes from MLGPRResult directly
- [Changed] datacube: check data types and metdata before adding/subtracting cubes
- [Changed] estimate_freqs_sefd(): add option to fit a poly in frequency
- [Changed] get_noise_std_cube: add option to account for the spatial tapering window applied to the data cube
- [Changed] ML-GPR: add possibility to use a NoiseStdCube as noise_cube
- [Changed] Adjust SKA Low primary beam
- [Changed] Add a ps_config option to full uniform spatial (aka k_per) binning scheme.
- [Fixed] pspec: fix filter_wedge_theta

## 0.28 - 2023-06-15

- [Fixed] Fix deprecation of numpy aliases of builtin types
- [Fixed] ml_gpr: fix c_nu_to_K when freqs is not uniformly sampled
- [Fixed] ml_gpr: improve CovarianceGenerator with optional k_par paddding and setting interpolcation kind

## 0.27 - 2023-05-19

- [Fixed] ml_gpr: fix product in list of kernels
- [Fixed] ml_gpr: fix covariance calculation of the VAE covariance with missing frequency channels

## 0.26 - 2023-04-28

- [New] Add initial support to retrieve the imaginary part of the cross-spectra.
- [New] scale_with_noise: add an alternative method to determine the UV scaling factor to match the weights to the input noise data cube
- [New] Add fill_gaps arguments to various MLGPRResult methods
- [New] Add the ml_gpr_config.kern.noise.estimate_baseline_noise_remove_n_pca settings which subtract n PCA component to the input noise cube which is then used to estimate the noise in the data
- [Changed] Update the LofarHBAPrimaryBeam to match better the simulation
- [Changed] Account for the smaller volume due to the window function in calculating the uncertainties of the cross-spectra.
- [Fixed] pstool make_ps: make sure an eor_bins_list is not required
- [Fixed] pstool gen_vis_cube: allow to load img_list file with only one fits file
- [Fixed] pscart.get_cross_power_spectra: The weight_cube should already be squared at this stage, so do not square it again

## 0.25 - 2023-03-01

- [Fixed] pstool simu_uv: setting the image resolution now set the oversampling factor correctly

## 0.24 - 2023-02-14

- [New] ml_gpr: add PreProcess class and a Fitter class that use it: VAEFitterProeProc
- [New] ml_gpr: adapt VAEKernTorch to make use of the ProProc class

## 0.23 - 2023-01-24

- [New] ml_gpr: add a get_result() method to load ultranest samples.
- [Changed] ml_gpr: removing clipping in c_nu_nu calculation.
- [Changed] ml_gpr: remove deprecated c_nu_from_ps21 functions.
- [Changed] ml_gpr: Discard nan and inf values in c_nu_nu.
- [Fixed] ml_gpr fix n_pick parameter not propagating correctly
- [Fixed] Fix frequency selection of CartImageCube.plot()
- [Fixed] When producing power-spectra from NoiseStdCube, take into account the spectral tapering window function which has not been applied to the cube, but is still corrected in the code.
- [Fixed] pstool even_odd_to_sum_diff: the weights needs to be summed not averaged.

The last change has a potential impact on SEFD estimation from data. When the visibility is computed from even/odd sets (using for example pspipe or pstool even_odd_to_sum_diff), the weights were not summed correctly previously, which lowered by sqrt(2) the SEFD estimates that would came out of data_cube.estimate_sefd() methods. The fix now set correctly set the weights for the sum and diff cube. To benefit from the fix, you will unfortunately need to generate the visibility cube again. This means running the pspipe gen_vis_cube task again, or pstool even_odd_to_sum_diff.

## 0.22 - 2022-12-05

- [Fixed] The use of the name 'hanning' for the 'hann' window function has been deprecated.

## 0.21 - 2022-11-28

- [Changed] Update dependencies

## 0.20 - 2022-11-06

- [New] Telescopes noise simulator: add HERA

## 0.19 - 2022-10-17

- [Fixed] Fix operator precedence when combining multiple kernels with ML-GPR configuration

## 0.18 - 2022-10-13

- [New] Add an alternative parametrization of the baseline dependence of the kernel coherence-scale: with wedge_parametrization set to True, the lengthscale and ls_alpha parameters are replaces by theta_rad. Power is confined to the wedge up to theta_rad.
- [New] Add a periodic kernel + refactoring of stationary kernel (include name change of MultiStationaryKern)
- [New] Add possibility to take the product of several MultiKern
- [New] Add an alternative nested sampler using ultranest
- [New] Add ThreePanelPsResults to visualize power-spectra results
- [Changed] Interpolate power-spectra in log scale to reduce interpolation errors when computing the freq-freq covariance from 1D power-spectra.
- [Fixed] Fix Log10Uniform prior transform
- [Fixed] Fix PowerSpectraConfig.copy() which was not working properly
- [Fixed] Fix Y label in SphericalPowerSpectraMC

## 0.17 - 2022-09-22

- [New] Add nested sampler option to run ml_gpr, refactor the way the sampler is run and saved (see instructions below to port your code).
- [New] BetaVAE: add an optional hidden layer in the mu/var branch of the encoder
- [New] Add the possibility to specify a noise cube (with option --noise_cube) when running gpr with pstool
- [New] Telescopes noise simulator: add MWA1
- [Changed] Various performance improvement of the ml_gpr module
- [Changed] pspec.get_ps3d(): respect umin/umax from configuration

#### Port code for new refactored ml_gpr sampler

Previous code to run MCMC (and save samples):

    sampler = ml_gpr.MCMCSampler(gp)
    sampler.run_mcmc(n_walkers, n_step, live_update=live_update, save_file=save_file, emcee_moves=emcee_moves)

must be now adapted like so:

    sampler = ml_gpr.MCMCSampler(gp, n_walkers, emcee_moves=emcee_moves)
    sampler_result = sampler.run(n_step, live_update=live_update)
    sampler_result.samples.save(save_file)

The methods `plot_samples()`, `plot_samples_likelihood()`, `plot_corner()`, `select_random_sample()`, `generate_data_cubes()`, `get_interpolated_i_cube()`, `get_ps_stack()` are now methods of the object `SamplerResult` which is returned by `sampler.run()` (and can be retrieved as well with the method `sampler.get_result()`). 

To plot the corner plot, you must now do:

    sampler_result.plot_corner()

To get the spherically-averaged power-spectra of component 'cmpt', you must now do:

    ps_stack = sampler_result.get_ps_stack(ps_gen, kbins, kern_name='cmpt')
    ps_stack.get_ps3d().plot()

To change the number of burned samples (for the MCMC sampler), you must now set it like that:

    sampler_result.samples.n_burn = 100

To load previously saved samples, you can create a new SamplerResult object like so:

    samples = ml_gpr.MCMCSamples.load(save_file)
    sampler_result = ml_gpr.SamplerResult(gp, samples)

The nested sampler is used the same way, just replace `ml_gpr.MCMCSampler` by `ml_gpr.NestedSampler` and `ml_gpr.MCMCSamples` by `ml_gpr.NestedSamples`.

## 0.16 - 2022-07-26

- [Fixed] Fix initialization of pspec.PowerSpectraBuilder
- [Fixed] Fix pspec.PsStacker.load
- [Changed] Silent warnings when metadata keys has >8 chars

## 0.15 - 2022-07-25

- [Changed] Improve PowerSpectraBuilder to give better default if no frequency range is given, and improve error/warning messages as well.
- [Fixed] Fix retrieving first/last slice with CartDataCube.plot_uv

## 0.14 - 2022-07-24

- [New] ml_gpr: refactor so that we can predict at different frequency than the input ones. It allows us to interpolate input cube and generate cube with filled frequency gaps.
- [New] pspec: add save/load option to pspec.PsStacker and pspec.SphericalPowerSpectraMC.save_to_txt()
- [Changed] by default CartDataCube.new_with_data will try to interpolate weights in the case the new freqs are different.
- [Changed] Allow the AbstractMLKern to generate covariance matrix with different X and X2.
- [Changed] ml_gpr: Add emcee_moves to ml_gpr config file.
- [Fixed] Set correct frequency channel width in ml_gpr.make_new_vis.
- [Fixed] datacube.CartImageCube.plot(fmhz='last') was not returning the right image.
- [Fixed] pspec: improve uncertainty calculation of cross-spectra

## 0.13 - 2022-02-18

- [Fixed] pspec: passing vmin/vmax and norm to imshow() is not allowed anymore.
- [Fixed] ml_gpr.get_samples: default to svd method which is more stable.
- [Fixed] ml_gpr.c_nu_from_ps21_fct: improve integration.
- [Changed] ml_gpr.AbstractMLKern.set_latent_space_normal_prior: add nsigma argument
- [Changed] ml_gpr.MCMCSampler.run_mcmc: add emcee_moves argument

## 0.12 - 2021-12-02

- [New] Add Aartfaac-A12-HBA and NenuFAR-full primary-beam and TelecopSimu object. Adapt the way the intra-baselines are discarded. Add --allow_intra_baselines option to pstool simu_uv. Add --fov=pb_fwhm option to pstool simu_uv
- [Fixed] Small cosmetic fix in SphericalPowerSpectraMC.plot()

## 0.11 - 2021-07-26

- [Fixed] Formatting issues.

## 0.10 - 2021-07-23

- [New] Add new ML-GPR fitting and power-spectra code that come in a new module: ml_gpr, new power-spectra class in pspec and a new command line pstool run_ml_gpr. This code is not yet stable and may change in the future. Also the ml_gpr may be moved to its own package in the future as well. Do not use it apart if you know what you are doing.
- [New] Add datacube.NoiseStdCube, datacube.CartWeightCube.get_noise_std_cube() to make a NoiseStdCube from a CartWeightCube, and code in pspec to generate power-spectra from it.
- [Changed] Move observation simulation code from pstool to obssimu.
- [Changed] Update SkaLow sefd.
- [New] Add pstool simu_ps_noise_zrange to generate noise curve for a specific k as function of redshift.

## 0.9 - 2021-05-06

- [Fixed] Speed up ps_gen.get_omega() by using caching key functions.
- [Fixed] Revert unintentional commit of work in progress code.


## 0.8  - 2021-04-30

- [Fixed] Fix the percentage of flagged data printed by CartDataCube.filter_min_weight().
- [Changed] Requires Python >= 3.6
