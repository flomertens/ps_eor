import sys

import GPy

from argparse import ArgumentParser

import matplotlib.pyplot as plt
import matplotlib as mpl

import numpy as np

from ps_eor import datacube, pspec, psutil, simu, fitutil, fgfit

parser = ArgumentParser(description="Run PS weighting test")
parser.add_argument('files_v', help='Input h5 V files', nargs='+')
parser.add_argument('--eor_bin', '-e', help='The EoR redshift bin', required=True, type=str)
parser.add_argument('--eor_bins_list', '-l', help='Listing of EoR redshift bins', default='eor_bins.txt')
parser.add_argument('--mode', '-m', help='Weighting mode', default='uv', type=str)
parser.add_argument('--n_files', '-n', help='Number of times we reuses the V files', default=1, type=int)

mpl.rcParams['image.cmap'] = 'rainbow'
mpl.rcParams['image.origin'] = 'lower'
mpl.rcParams['image.interpolation'] = 'nearest'
mpl.rcParams['axes.grid'] = True

fg_ls = 50
fg_med_ls = 4
fg_var = 40e-4
fg_med_var = 10e-4
eor_var = 1e-7
seed_n = 1
kbin = [0.07, 0.17]


def main():
    args = parser.parse_args(sys.argv[1:])

    def _fmhz(freqs):
        return np.round(freqs * 1e-6, 1)

    files = args.files_v
    files = files * args.n_files
    ft_V_cube = datacube.CartDataCube.load(files[0])

    eor_bin_list = pspec.EorBinList.load(args.eor_bins_list)
    eor = eor_bin_list.get(args.eor_bin, freqs=ft_V_cube.freqs)

    np.random.seed(seed_n)

    fg_gen = simu.SimuGPCube(np.arange(eor.freqs[0] - 2 * eor.df, eor.freqs[-1] + 2 * eor.df, eor.df),
                             GPy.kern.Matern52(1), ft_V_cube.meta.res, ft_V_cube.meta.theta_fov, 50, 250)
    fg_cube = fg_gen.get(fg_var, fg_ls)

    eor_gen = simu.SimuGPCube(np.arange(eor.freqs[0] - 2 * eor.df, eor.freqs[-1] + 2 * eor.df, eor.df),
                              GPy.kern.Exponential(1), ft_V_cube.meta.res, ft_V_cube.meta.theta_fov, 50, 250)
    eor_cube = eor_gen.get(eor_var, 0.8)

    weighting_mode = args.mode
    combiner = datacube.DataCubeCombiner(weighting_mode=weighting_mode)
    combiner_v = datacube.DataCubeCombiner(weighting_mode=weighting_mode)

    all_ps = []

    m = len(files)
    pr = psutil.progress_report(m)

    eor = eor_bin_list.get(args.eor_bin, freqs=eor_cube.freqs)
    el = 2 * np.pi * (np.arange(eor_cube.ru.min(), eor_cube.ru.max(), 10))
    ps_conf = pspec.PowerSpectraConfig(el, window_fct='hann')
    ps_conf.weights_by_default = True
    ps_conf.ps2d_pos_only = True
    ps_gen = pspec.PowerSpectraCart(eor, ps_conf, datacube.LofarHBAPrimaryBeam())

    ps = ps_gen.get_ps3d(kbin, eor_cube)

    for i, file in enumerate(files):
        np.random.seed(seed_n + i)
        pr(i)
        ft_V_cube = eor.get_slice(datacube.CartDataCube.load(file))
        noise_cube1 = ft_V_cube.weights.simulate_noise(4000, 12 * 3600)
        # noise_cube2 = ft_V_cube.weights.simulate_noise(4000, 12 * 3600)
        idx_freqs = np.in1d(_fmhz(fg_cube.freqs), _fmhz(noise_cube1.freqs))
        fg_med_cube = fg_gen.get(10e-4, 2)
        fg = fg_cube.get_slice_from_idx(idx_freqs) + fg_med_cube.get_slice_from_idx(idx_freqs)
        obs_cube = fg + eor_cube.get_slice_from_idx(idx_freqs) + noise_cube1
        obs_cube.set_weights(noise_cube1.weights)
        combiner.add(obs_cube)
        combiner_v.add(noise_cube1)

        c = combiner.get()
        n = combiner_v.get()

        outliers = psutil.get_weights_outliers(n.freqs, n.weights.get(), 0.9)
        outliers += psutil.get_outliers(n.freqs, n.data, threshold_n_rms=2)
        c.filter_outliers(outliers)
        c.filter_uvrange(50, 250)
        c.filter_min_weight(min_weight=0.1 * c.weights.get().min(axis=0).max())
        n.filter_outliers(outliers)
        n.filter_uvrange(50, 250)
        n.filter_min_weight(min_weight=0.1 * c.weights.get().min(axis=0).max())

        gpr_config = fitutil.GprConfig()
        gpr_config.fg_kern = GPy.kern.Matern52(1, name='fg_rbf') + GPy.kern.Matern52(1, name='fg_rbf')
        gpr_config.fg_kern.fg_rbf.lengthscale.constrain_bounded(5, 200)
        gpr_config.fg_kern.fg_rbf_1.lengthscale.constrain_bounded(1, 10)

        gpr_config.eor_kern = GPy.kern.Exponential(1, name='eor')
        gpr_config.eor_kern.lengthscale.set_prior(GPy.priors.Gamma.from_EV(.85, 0.2))

        gpr_config.num_restarts = 20
        gpr_config.fixed_noise = False
        gpr_config.fixed_noise_scale = 1
        gpr_config.first_run_pca_n = 0
        gpr_config.verbose = False

        fitter = fgfit.GprForegroundFit(gpr_config)
        fit_res = fitter.run(c, n)

        eor = eor_bin_list.get(args.eor_bin, freqs=n.freqs)
        c.freqs = n.freqs
        ps_gen = pspec.PowerSpectraCart(eor, ps_conf, datacube.LofarHBAPrimaryBeam())

        all_ps.append(ps_gen.get_ps3d_with_noise(kbin, fit_res.sub, n))

    ps_d = np.array([k.data[0] * 1e6 for k in np.array(all_ps).flatten()])
    ps_d_e = np.array([k.err[0] * 1e6 for k in np.array(all_ps).flatten()])

    plt.plot(np.arange(m) + 1, ps_d)
    plt.fill_between(np.arange(m) + 1, (ps_d - 2 * ps_d_e), (ps_d + 2 * ps_d_e), alpha=0.4)
    plt.axhline(ps.data[0] * 1e6, ls='--', c=psutil.black)
    plt.yscale('log')
    plt.xlabel('nights')
    plt.ylim(bottom=1e1)
    plt.ylabel('$\Delta^2 (k)\,[\mathrm{mK^2}]$ at k = 0.1')
    plt.tight_layout()
    plt.savefig('test_ps_combine_with_fg_nights_w-%s.pdf' % args.mode)

    eor = eor_bin_list.get(args.eor_bin, freqs=n.freqs)
    c.freqs = n.freqs
    el = 2 * np.pi * (np.arange(c.ru.min(), c.ru.max(), 10))
    ps_gen = pspec.PowerSpectraCart(eor, ps_conf, datacube.LofarHBAPrimaryBeam())

    idx_freqs = np.in1d(_fmhz(eor_cube.freqs), _fmhz(n.freqs))

    plt.figure()
    kbins = np.logspace(np.log10(ps_gen.kmin), np.log10(0.8), num=8)
    ps_gen.get_ps3d(kbins, fit_res.fit).plot()
    ps_gen.get_ps3d(kbins, eor_cube.get_slice_from_idx(idx_freqs)).plot(ax=plt.gca(), ls='--', c=psutil.black)
    ps_gen.get_ps3d(kbins, n).plot(ax=plt.gca())
    ps_gen.get_ps3d(kbins, fit_res.sub).plot(ax=plt.gca())
    ps_gen.get_ps3d_with_noise(kbins, fit_res.sub, n).plot(ax=plt.gca())
    plt.ylim(bottom=1e1)
    plt.tight_layout()
    plt.savefig('test_ps_combine_with_fg_last_w-%s.pdf' % args.mode)


if __name__ == '__main__':
    main()
