import sys

from argparse import ArgumentParser

import matplotlib.pyplot as plt
import matplotlib as mpl

import numpy as np

from ps_eor import datacube, pspec, psutil

parser = ArgumentParser(description="Run PS weighting test")
parser.add_argument('--file_v', '-v', help='Stokes V visibility cube in h5 format', required=True)
parser.add_argument('--eor_bin', '-e', help='The EoR redshift bin', required=True, type=str)
parser.add_argument('--eor_bins_list', '-l', help='Listing of EoR redshift bins', default='eor_bins.txt')
parser.add_argument('--n_mc', '-n', help='Number of MC simulations', default=2000, type=int)
parser.add_argument('--snr', '-s', help='SNR of signal', default=1, type=float)

mpl.rcParams['image.cmap'] = 'rainbow'
mpl.rcParams['image.origin'] = 'lower'
mpl.rcParams['image.interpolation'] = 'nearest'
mpl.rcParams['axes.grid'] = True


def main():
    args = parser.parse_args(sys.argv[1:])

    ft_V_cube = datacube.CartDataCube.load(args.file_v)
    ft_V_cube.filter_uvrange(50, 250)

    eor_bin_list = pspec.EorBinList.load(args.eor_bins_list)

    eor = eor_bin_list.get(args.eor_bin, freqs=ft_V_cube.freqs)

    el = 2 * np.pi * (np.arange(ft_V_cube.ru.min(), ft_V_cube.ru.max(), 10))
    ps_conf = pspec.PowerSpectraConfig(el)
    ps_conf.weights_by_default = True
    ps_conf.psf_weights_square = True
    ps_conf.window_fct = 'boxcar'
    ps_conf.rmean_axis = None
    ps_gen = pspec.PowerSpectraCart(eor, ps_conf, datacube.LofarHBAPrimaryBeam())

    ft_V_cube.weights.unscale()
    ft_V_cube.weights.scale_with_noise_cube(eor.get_slice(ft_V_cube))
    ft_V_cube.weights.random_scale()

    uniform_weights = ft_V_cube.weights.copy()
    uniform_weights.unscale()
    uniform_weights.data = np.ones_like(uniform_weights.data) * np.mean(uniform_weights.data)

    uni_noise_cube = uniform_weights.simulate_noise(args.snr * 4000, 12 * 3600, True)
    uni_noise_cube.set_weights(None)

    kbins = np.logspace(np.log10(ps_gen.kmin), np.log10(0.6), num=11)

    ps = ps_gen.get_ps3d(kbins, uni_noise_cube)

    all_rec = []
    all_rec_nw = []
    all_rec_ww = []
    all_err = []
    all_err_nw = []
    all_err_ww = []

    n = args.n_mc
    pr = psutil.progress_report(n)

    for i in range(n):
        pr(i)

        noise_cube1 = ft_V_cube.weights.simulate_noise(4000, 12 * 3600, True)
        noise_cube2 = ft_V_cube.weights.simulate_noise(4000, 12 * 3600, True)

        obs_cube = uni_noise_cube + noise_cube1

        ps_rec = ps_gen.get_ps3d_with_noise(kbins, obs_cube, noise_cube2)
        ps_rec_nw = ps_gen.get_ps3d_with_noise(kbins, obs_cube, noise_cube2, weighted=False)

        noise_cube2.weights.random_scale()
        obs_cube.set_weights(noise_cube2.weights)

        ps_rec_ww = ps_gen.get_ps3d_with_noise(kbins, obs_cube, noise_cube2)

        all_rec.append(ps_rec.data)
        all_err.append(ps_rec.err)

        all_rec_nw.append(ps_rec_nw.data)
        all_err_nw.append(ps_rec_nw.err)

        all_rec_ww.append(ps_rec_ww.data)
        all_err_ww.append(ps_rec_ww.err)

    all_rec = np.array(all_rec)
    all_rec_nw = np.array(all_rec_nw)
    all_rec_ww = np.array(all_rec_ww)
    all_err = np.array(all_err)
    all_err_nw = np.array(all_err_nw)
    all_err_ww = np.array(all_err_ww)

    c1 = plt.cm.Set2.colors[0]
    c2 = plt.cm.Set2.colors[2]
    c3 = plt.cm.Set2.colors[1]

    plt.figure()
    plt.plot(ps.k_mean, np.sqrt(np.median(all_err_ww, axis=0) ** 2 /
                                (all_rec_ww.var(axis=0) + ps.err ** 2)) - 1, label='wrong weights', c=c1)
    plt.plot(ps.k_mean, np.sqrt(np.median(all_err_nw, axis=0) ** 2 /
                                (all_rec_nw.var(axis=0) + ps.err ** 2)) - 1, label='unweighted', c=c2)
    plt.plot(ps.k_mean, np.sqrt(np.median(all_err, axis=0) ** 2 /
                                (all_rec.var(axis=0) + ps.err ** 2)) - 1, label='weighted', c=c3)
    plt.xscale('log')
    plt.ylim(-0.5, 0.5)
    plt.xlabel('k')
    plt.ylabel('Relative error on PS uncertainty')
    plt.legend()
    plt.tight_layout()
    plt.savefig('test_ps_uncertainty_err.pdf')

    p = np.median(all_rec, axis=0)
    q16 = np.percentile(all_rec, 16, axis=0)
    q84 = np.percentile(all_rec, 84, axis=0)
    # e = np.median(all_err, axis=0)

    p2 = np.median(all_rec_nw, axis=0)
    # e2 = np.median(all_err_nw, axis=0)
    q162 = np.percentile(all_rec_nw, 16, axis=0)
    q842 = np.percentile(all_rec_nw, 84, axis=0)

    p3 = np.median(all_rec_ww, axis=0)
    # e3 = np.median(all_err_ww, axis=0)
    q163 = np.percentile(all_rec_ww, 16, axis=0)
    q843 = np.percentile(all_rec_ww, 84, axis=0)

    plt.figure()
    plt.plot(ps.k_mean, p3 / ps.data - 1, label='wrong weights', c=c1)
    plt.fill_between(ps.k_mean, q163 / ps.data - 1, q843 / ps.data - 1, alpha=0.3, color=c1)
    # plt.fill_between(ps.k_mean, (p3 - e3) / ps.data - 1, (p3 + e3) / ps.data - 1, alpha=0.3, color=c3)
    plt.plot(ps.k_mean, p2 / ps.data - 1, label='unweighted', c=c2)
    plt.fill_between(ps.k_mean, q162 / ps.data - 1, q842 / ps.data - 1, alpha=0.3, color=c2)
    plt.plot(ps.k_mean, p / ps.data - 1, label='weighted', c=c3)
    plt.fill_between(ps.k_mean, q16 / ps.data - 1, q84 / ps.data - 1, alpha=0.3, color=c3)
    plt.xscale('log')
    plt.ylim(-0.7, 0.7)
    plt.xlabel('k')
    plt.ylabel('Relative error on PS')
    plt.legend()
    plt.tight_layout()
    plt.savefig('test_ps_err.pdf')


if __name__ == '__main__':
    main()
