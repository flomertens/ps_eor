import sys

from argparse import ArgumentParser

import matplotlib.pyplot as plt
import matplotlib as mpl

import numpy as np

from ps_eor import datacube, pspec, psutil

parser = ArgumentParser(description="Run PS weighting test")
parser.add_argument('--file_v', '-v', help='Stokes V visibility cube in h5 format', required=True)
parser.add_argument('--eor_bin', '-e', help='The EoR redshift bin', required=True, type=str)
parser.add_argument('--eor_bins_list', '-l', help='Listing of EoR redshift bins', default='eor_bins.txt')
parser.add_argument('--n_nights', '-n', help='Number of combined nights', default=200, type=int)
parser.add_argument('--snr', '-s', help='SNR of signal', default=0.04, type=float)

mpl.rcParams['image.cmap'] = 'rainbow'
mpl.rcParams['image.origin'] = 'lower'
mpl.rcParams['image.interpolation'] = 'nearest'
mpl.rcParams['axes.grid'] = True

random_scale_ratio = 2
weights_uncertainity_ratio = 1.2
tt_min = 10
tt_max = 16


def main():
    args = parser.parse_args(sys.argv[1:])

    ft_V_cube = datacube.CartDataCube.load(args.file_v)

    outliers = psutil.get_outliers(ft_V_cube.freqs, ft_V_cube.data, threshold_n_rms=4)
    print 'Filtering outliers:', ft_V_cube.freqs[outliers] * 1e-6
    ft_V_cube.filter_outliers(outliers)

    ft_V_cube.filter_uvrange(50, 250)

    eor_bin_list = pspec.EorBinList.load(args.eor_bins_list)

    eor = eor_bin_list.get(args.eor_bin, freqs=ft_V_cube.freqs)

    el = 2 * np.pi * (np.arange(ft_V_cube.ru.min(), ft_V_cube.ru.max(), 10))
    ps_conf = pspec.PowerSpectraConfig(el)
    ps_conf.weights_by_default = True
    ps_conf.window_fct = 'boxcar'
    ps_conf.rmean_axis = None
    ps_gen = pspec.PowerSpectraCart(eor, ps_conf, datacube.LofarHBAPrimaryBeam())

    ft_V_cube.weights.unscale()
    ft_V_cube.weights.scale_with_noise_cube(eor.get_slice(ft_V_cube))
    ft_V_cube.weights.random_scale()

    uniform_weights = ft_V_cube.weights.copy()
    uniform_weights.unscale()
    uniform_weights.data = np.ones_like(uniform_weights.data) * np.mean(uniform_weights.data)

    uni_noise_cube = uniform_weights.simulate_noise(args.snr * 4000, 12 * 3600, True)
    uni_noise_cube.set_weights(None)

    kbins = np.logspace(np.log10(ps_gen.kmin), np.log10(0.6), num=11)

    ps = ps_gen.get_ps3d(kbins, uni_noise_cube)

    modes = ['full', 'global']

    combiners = [datacube.DataCubeCombiner(weighting_mode=mode) for mode in modes]
    combiners_v = [datacube.DataCubeCombiner(weighting_mode=mode) for mode in modes]

    n = args.n_nights
    pr = psutil.progress_report(n)

    all_ps = []
    all_ps_v = []
    all_ps_d = []

    for i in range(n):
        pr(i)
        ft_V_cube.weights.random_scale(random_scale_ratio)
        tt = np.random.uniform(tt_min, tt_max)
        noise_cube1 = ft_V_cube.weights.simulate_noise(
            4000, tt * 3600, False, weights_uncertainity_ratio=weights_uncertainity_ratio)
        noise_cube2 = ft_V_cube.weights.simulate_noise(
            4000, tt * 3600, False, weights_uncertainity_ratio=weights_uncertainity_ratio)
        obs_cube = uni_noise_cube + noise_cube1

        for (combiner, combiner_v) in zip(combiners, combiners_v):
            combiner.add(obs_cube)
            combiner_v.add(noise_cube2)

        all_ps.append([ps_gen.get_ps3d([0.008, 0.12], c.get()).data[0] * 1e6 for c in combiners])
        all_ps_v.append([ps_gen.get_ps3d([0.008, 0.12], c.get()).data[0] * 1e6 for c in combiners_v])
        all_ps_d.append([ps_gen.get_ps3d_with_noise([0.008, 0.12], c.get(), c_v.get())
                         for c, c_v in zip(combiners, combiners_v)])

    ps = ps_gen.get_ps3d([0.008, 0.12], uni_noise_cube).data[0] * 1e6

    ps_d = np.reshape([k.data[0] * 1e6 for k in np.array(all_ps_d).flatten()], np.array(all_ps_d).shape)
    ps_d_e = np.reshape([k.err[0] * 1e6 for k in np.array(all_ps_d).flatten()], np.array(all_ps_d).shape)

    all_ps = np.array(all_ps)
    all_ps_v = np.array(all_ps_v)

    for i, mode in enumerate(modes):
        plt.plot(np.arange(n) + 1, all_ps[:, i], label='I %s' % mode)
        plt.plot(np.arange(n) + 1, all_ps_v[:, i], label='V %s' % mode)
    plt.plot(np.arange(n) + 1, all_ps[0, 0] * 1 / (np.arange(n) + 1), ls=':', c=psutil.lblack)
    plt.axhline(ps, ls='--', c=psutil.black, label='signal')
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel('nights')
    plt.ylabel('$\Delta^2 (k)\,[\mathrm{mK^2}]$ at k = 0.1')
    plt.legend()
    plt.tight_layout()
    plt.savefig('test_ps_combine_nights.pdf')

    fig, axs = plt.subplots(ncols=len(modes), nrows=1, figsize=(4 * len(modes), 3), sharey=True)
    for i, mode in enumerate(modes):
        axs[i].plot(np.arange(n) + 1, ps_d[:, i], label=mode)
        axs[i].fill_between(np.arange(n) + 1, (ps_d - 2 * ps_d_e)[:, i], (ps_d + 2 * ps_d_e)[:, i], alpha=0.4)
        axs[i].axhline(ps, ls='--', c=psutil.black)
        axs[i].set_yscale('log')
        axs[i].set_xlabel('nights')
        axs[i].set_ylabel('$\Delta^2 (k)\,[\mathrm{mK^2}]$ at k = 0.1')
        axs[i].legend()

    fig.tight_layout()
    fig.savefig('test_ps_combine_nights_detection.pdf')


if __name__ == '__main__':
    main()
